get:
	fvm flutter pub get

remove:
	fvm flutter packages pub remove $(p)

add:
	fvm flutter packages pub add $(p)

sort:
	fvm flutter pub run import_sorter:main -e

fix:
	dart fix --apply

format:
	make sort
	make fix

build:
	fvm flutter packages pub run build_runner build --delete-conflicting-outputs

.PHONY: dev get remove build sort format