// 🐦 Flutter imports:

// 🎯 Dart imports:
import 'dart:async';

// 🐦 Flutter imports:
import 'package:flutter/material.dart';

// 📦 Package imports:
import 'package:easy_localization/easy_localization.dart';
import 'package:video_player/video_player.dart';

// 🌎 Project imports:
import 'package:hls_streaming_app/core/core.dart';

const duration = Duration(seconds: 3);

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late VideoPlayerController _firstController;
  late VideoPlayerController _secondController;

  bool get isControllersInited =>
      _firstController.value.isInitialized &&
      _secondController.value.isInitialized;

  @override
  void initState() {
    super.initState();

    initControllers();
  }

  Future<void> initControllers() async {
    _firstController = initController(1);
    _secondController = initController(2);

    Timer.periodic(duration, (timer) {
      equalizeControllerTiming();
    });
  }

  VideoPlayerController initController(int id) {
    return VideoPlayerController.networkUrl(
      Endpoints.getCameraData(id).asUri,
      formatHint: VideoFormat.hls,
      videoPlayerOptions: VideoPlayerOptions(
        mixWithOthers: true,
        allowBackgroundPlayback: true,
      ),
    )..initialize().then((_) {
        setState(() {
          if (isControllersInited) playVideos();
        });
      });
  }

  void equalizeControllerTiming() {
    final position1 = _firstController.value.position;
    final position2 = _secondController.value.position;

    final positionDifference = (position1 - position2).inMilliseconds.abs();

    if (positionDifference > duration.inMilliseconds) {
      if (_firstController.value.position > _secondController.value.position) {
        _secondController.seekTo(_firstController.value.position);
      } else {
        _firstController.seekTo(_secondController.value.position);
      }
    }
  }

  Future<void> playVideos() async {
    await _firstController.seekTo(Duration.zero);
    await _secondController.seekTo(Duration.zero);

    await _firstController.play();
    await _secondController.play();
  }

  @override
  void dispose() {
    _firstController.dispose();
    _secondController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('app_name'.tr()),
      ),
      body: isControllersInited
          ? Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AspectRatio(
                  aspectRatio: _firstController.value.aspectRatio,
                  child: VideoPlayer(_firstController),
                ),
                const SizedBox(height: 16),
                AspectRatio(
                  aspectRatio: _secondController.value.aspectRatio,
                  child: VideoPlayer(_secondController),
                ),
              ],
            )
          : const Center(child: CircularProgressIndicator()),
    );
  }
}
