abstract class Endpoints {
  static const baseApi = 'https://dev-api-video.u-prox.systems:443/api/v1/';

  static String getCameraData(int stream) =>
      '${baseApi}event/test/playback/masterPlaylist.m3u8?streamNumber=$stream';
}

extension StringExt on String {
  Uri get asUri => Uri.parse(this);
}
