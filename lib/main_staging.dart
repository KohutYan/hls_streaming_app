// 🌎 Project imports:
import 'package:hls_streaming_app/app/app.dart';
import 'package:hls_streaming_app/bootstrap.dart';

void main() {
  bootstrap(() => const App());
}
