# Hls Streaming App

Test app

---

## Getting Started 🚀

This project contains 3 flavors:

- development
- staging
- production

To run the desired flavor either use the launch configuration in VSCode/Android Studio or use the
following commands:

```sh
# Development
$ flutter run --flavor development --target lib/main_development.dart

# Staging
$ flutter run --flavor staging --target lib/main_staging.dart

# Production
$ flutter run --flavor production --target lib/main_production.dart
```

_\*Hls Streaming App works on iOS, Android, Web, and Windows._

---

## Working with Translations 🌐

This project relies on [easy_localization](https://pub.dev/packages/easy_localization)

### Adding Strings

1. To add a new localizable string, open the `{languageCode}.json` file at `assets/translations/`.

2. Then add a new key/value

```dart
import 'package:easy_localization/easy_localization.dart';

@override
Widget build(BuildContext context) {
  return Text(helloWorld.tr());
}
```

### Adding Supported Locales

Update the `CFBundleLocalizations` array in the `Info.plist` at `ios/Runner/Info.plist` to include
the new locale.

```xml
    ...

<key>CFBundleLocalizations</key><array>
<string>en</string>
<string>es</string>
</array>

    ...
```

### Adding Translations

1. For each supported locale, add a new ARB file in `assets/translations`.

```
├── assets
│   ├── translations
│   │   ├── en.json
│   │   └── es.json
```

2. Add the translated strings to each `.json` file:

### Warmup shaders

1. Run `puro flutter run --profile --cache-sksl --purge-persistent-cache`
2. Build ios ipa as described in previous section
   For more information check [Shader compilation jank](https://docs.flutter.dev/perf/shader)

### Run Code generation

1. In the terminal enter `make build`
   This command will generate  `*.g.dart`, `*.freezed.dart`, etc. as output
   Look Makefile for more details

### Before commit

1. Ensure you fixed all warnings and errors
2. Run `make format` to format code
3. Branch name should be `feature/[DASH-TASK-NUMBER]/feature_name`
   or `bugfix/[DASH-TASK-NUMBER]/fix_name`
4. Commit should begin with prefix: `feat:`, `fix:` or `refactor:`. Look more for
   details [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)
